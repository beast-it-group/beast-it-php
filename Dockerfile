ARG BASE_IMAGE=php:8.1-fpm-alpine
FROM ${BASE_IMAGE}

# TODO: harden image (eg: https://gist.github.com/jumanjiman/f9d3db977846c163df12)

# TODO: arguments on-build (Build base image in registry w/ all extensions)
# TODO: optimize PHP config (COPY ./www.tweaks/conf /usr/local/etc/php-fpm.d/)

WORKDIR /var/www

ARG APCU=y
ARG INTL=y
ARG GD=y
ARG GIT=y
ARG IMAGICK=n
ARG MYSQLI=y
ARG OPCACHE=y
ARG PDO_MYSQL=y
ARG REDIS=y
ARG XDEBUG=n
ARG XSL=y
ARG ZIP=y

ARG ENABLE_COMPOSER=n

RUN apk add --update --no-cache --virtual build-dependencies \
      bash \
      gnupg \
      wget \
      $PHPIZE_DEPS \
    && if [[ "$APCU"="y" ]]; then \
      pecl install apcu \
      && docker-php-ext-enable apcu \
      && pecl clear-cache; \
    fi \
    && if [[ "$ENABLE_COMPOSER" = "y" ]] ; then \
      apk add curl \
      && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer; \
    fi \
    && if [[ "$INTL"="y" ]]; then \
      apk add --no-cache icu-dev \
      && docker-php-ext-configure intl \
      && docker-php-ext-install intl; \
    fi \
    && if [[ "$GD"="y" ]]; then \
      apk add --no-cache freetype-dev imagemagick imagemagick-dev libjpeg-turbo-dev \
      && docker-php-ext-configure gd --with-jpeg --with-freetype \
      && docker-php-ext-install gd; \
    fi \
    && if [[ "$GIT"="y" ]]; then \
      apk add --no-cache git; \
    fi \
    && if [[ "$IMAGICK"="y" ]]; then \
      pecl install imagick \
      && docker-php-ext-enable imagick; \
    fi \
    && if [[ "$MYSQLI"="y" ]]; then \
      docker-php-ext-configure mysqli \
      && docker-php-ext-install mysqli; \
    fi \
    && if [[ "$OPCACHE"="y" ]]; then \
      docker-php-ext-configure opcache \
      && docker-php-ext-install opcache; \
    fi \
    && if [[ "$PDO_MYSQL"="y" ]]; then \
      docker-php-ext-install mysqli pdo pdo_mysql \
      && docker-php-ext-enable pdo_mysql; \
    fi \
    && if [[ "$REDIS"="y" ]]; then \
      pecl install redis \
      && docker-php-ext-enable redis; \
    fi \
    && if [[ "$XDEBUG"="y" ]]; then \
      pecl install xdebug \
      && docker-php-ext-enable xdebug; \
    fi \
    && if [[ "$XSL"="y" ]]; then \
      apk add --no-cache libxslt-dev \
      && docker-php-ext-install xsl \
      && docker-php-ext-enable xsl; \
    fi \
    && if [[ "$ZIP"="y" ]]; then \
      apk add libzip-dev zip \
      && docker-php-ext-configure zip \
      && docker-php-ext-install zip; \
    fi \
    && wget https://get.symfony.com/cli/installer -O - | bash \
    && mv /root/.symfony/bin/symfony /usr/local/bin/symfony \
    && apk del build-dependencies

EXPOSE 9000

CMD php-fpm
